<?php

namespace Symfony\UX\Cropper;

use Intervention\Image\ImageManager;
use Symfony\UX\Cropper\Model\Crop;

class Cropper
{
    private $imageManager;

    public function __construct(ImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    public function createCrop(string $filename): Crop
    {
        return new Crop($this->imageManager, $filename);
    }
}
