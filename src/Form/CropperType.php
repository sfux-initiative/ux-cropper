<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\UX\Cropper\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\UX\Cropper\Model\Crop;

/**
 * @author Titouan Galopin <galopintitouan@gmail.com>
 *
 * @final
 */
class CropperType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('options', HiddenType::class, [
                'error_bubbling' => true,
                'attr' => [
                    'data-controller' => '@symfony/ux-cropper/cropper',
                    'data-public-url' => $options['public_url'],
                    'data-aspect-ratio' => $options['aspect_ratio'] ?: '',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('public_url');
        $resolver->setAllowedTypes('public_url', 'string');

        $resolver->setDefined('aspect_ratio');
        $resolver->setAllowedTypes('aspect_ratio', ['double', 'null']);

        $resolver->setDefaults([
            'label' => false,
            'data_class' => Crop::class,
            'attr' => [
                'class' => 'cropper',
            ],
            'aspect_ratio' => null,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'cropper';
    }
}
