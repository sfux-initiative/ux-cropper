import { Controller } from 'stimulus';
import Cropper from 'cropperjs';

export default class extends Controller {
    connect() {
        // Create image view
        const img = document.createElement('img');
        img.classList.add('cropper-image');
        img.style.maxWidth = '100%';
        img.src = this.element.getAttribute('data-public-url');

        const parent = this.element.parentNode;
        parent.appendChild(img);

        // Build the cropper
        let options = {
            movable: false,
            rotatable: false,
            scalable: false,
            zoomable: false,
            crop: (event) => {
                this.element.value = JSON.stringify(event.detail);
            },
        };

        if (this.element.getAttribute('data-aspect-ratio')) {
            options.aspectRatio = parseFloat(this.element.getAttribute('data-aspect-ratio'));
        }

        new Cropper(img, options);
    }
}
